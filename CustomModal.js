import React, { Component } from 'react';
import { StyleSheet, Text, View, Modal, Image, Button, Alert } from 'react-native';

export default class CustomModal extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.img}
          source={{uri: `${this.props.img}`}}
        />
        <Text style={{textAlign: 'center'}}>
          {this.props.children}
        </Text>
        {this.props.buttonPositive}
        {this.props.buttonNegative}
        {this.props.buttonNeutral}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    width: 250,
    height: 250,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 35,
    backgroundColor: '#fff',
  },

  img: {
    width: 50,
    height: 50,
    marginBottom: 15,
  },
})