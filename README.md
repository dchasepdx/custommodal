#CustomModal

The image can be set with an img prop, and the text is the children of the component. There are 3 options for buttons that can be passed as the following props, buttonPositive, button, negative, and buttonNeutral. The component is in CustomModal.js. The App.js file is just a top level component that let me see what it would look like.