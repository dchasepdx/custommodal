import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import CustomModal from './CustomModal';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <Text>Changes you make will automatically reload.</Text>
        <Text>Shake your phone to open the developer menu.</Text>
        <CustomModal
          img='https://facebook.github.io/react/img/logo_og.png'
          buttonPositive={
            <Button
              style={styles.tryAgain} 
              title='Try again'
              onPress={() => true}
            />
          }
          buttonNeutral={
            <Button 
              title='Cancel'
              onPress={() => true}
            />
          }
        >
          Please turn on your Bluetooth connection to connect your CARFIT PULS sensor.
          </CustomModal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
